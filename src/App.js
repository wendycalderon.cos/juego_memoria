import React, { Component } from "react";
import Header from "./components/Header.js";
import Tablero from "./components/Tablero.js";
import "./App.css";
import construirBaraja from "./utils/construirBaraja";

const getEstadoInicial = () => {
  const baraja = construirBaraja();
  return {
    baraja,
    parejaSeleccionada: [],
    esComparada: false,
    numeroIntentos:0
  };
};

class App extends Component {
  constructor(props) {
    super(props);
    this.state = getEstadoInicial();
  }

  render() {
    return (
      <div className="App">
        <Header numeroIntentos={this.state.numeroIntentos} resetearPartida={()=>this.resetearPartida()}></Header>
        <Tablero
          baraja={this.state.baraja}
          parejaSeleccionada={this.state.parejaSeleccionada}
          seleccionarCarta={carta => this.seleccionarCarta(carta)}
        ></Tablero>
      </div>
    );
  }

  seleccionarCarta(carta) {
    if (
      this.esComparada ||
      this.state.parejaSeleccionada.indexOf(carta) > -1 ||
      carta.fueAdivinada
    ) {
      return;
    }

    const parejaSeleccionada = [...this.state.parejaSeleccionada, carta];

    this.setState({
      parejaSeleccionada
    });

    if (parejaSeleccionada.length === 2) {
      this.compararPareja(parejaSeleccionada);
    }
  }
  compararPareja(parejaSeleccionada) {
    this.setState({
      estaComparando: true
    });

    setTimeout(() => {
      const [primera, segunda] = parejaSeleccionada;
      let baraja = this.state.baraja;

      if (primera.icono === segunda.icono) {
        baraja = baraja.map(carta => {
          if (carta.icono !== primera.icono) {
            return carta;
          }
          return { ...carta, fueAdivinada: true };
        });
      }
      this.verificarGanador(baraja);
      this.setState({
        parejaSeleccionada: [],
        baraja,
        estaComparando: false,
        numeroIntentos: this.state.numeroIntentos+1,
      });
    }, 600);
  }

  verificarGanador(baraja) {
    if (baraja.filter(carta => !carta.fueAdivinada).length === 0) {
      alert(`Ganaste en ${this.state.numeroIntentos} intentos`);
    }
  }

  resetearPartida(){
    this.setState(
      getEstadoInicial(),
    );
  }
}

export default App;
