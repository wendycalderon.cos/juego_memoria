import React, { Component } from "react";
import "./Header.css";

export default class Header extends Component {
  render() {
    return (
      <header>
        <div className="titulo">Memory Game</div>
        <div>
          <button className="boton-reiniciar" onClick={this.props.resetearPartida}>Reiniciar</button>
        </div>
        <div className="titulo">
          Numero de intentos = {this.props.numeroIntentos}
        </div>
      </header>
    );
  }
}
